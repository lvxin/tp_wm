/**
 * Created by lvxin on 2016/6/5.
 */
(function ($) {

    var player1 = {

        p1: $('.player1'),

        init:function () {
            this.p1.animate({opacity:1},1500,'linear');
        },
        active:function(animate_str){
            this.p2.addClass("animated " + animate_str);
            setTimeout("$('.player1').removeClass().addClass('player1')",1000);
        },
        over:function(){
            this.p1.animate({left:-260,opacity:0},300,'linear');
        }
    };
    
    var player2 = {
        p2: $('.player2'),

        init:function () {
            this.p2.animate({left:550},800,'linear');
        },
        active:function(animate_str){
            this.p2.addClass("animated " + animate_str);
            setTimeout("$('.player2').removeClass().addClass('player2')",1000);
        },
        over:function(){
            this.p2.animate({top:200,height:0,opacity:0},400,'linear');
            setTimeout("$('.player2').remove()",500);
        }
    };

    var control_panel = {
        panel: $('.control-panel'),

        show:function () {
            this.panel.animate({top:450},300,'linear');
        },

        hidden:function () {
            this.panel.animate({top:600},300,'linear');
        }
    };


    player1.init();
    player2.init();
    control_panel.show();


    $('#test1').click(function(){
        player2.active('bounce');
    });

    $('#test2').click(function(){
        player2.over();
    });

    $('#test3').click(function(){
        control_panel.hidden();
    });


})(jQuery);