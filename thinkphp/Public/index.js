/**
 * Created by lvxin on 2016/6/1.
 */

(function ($) {

    var socket = new WebSocket('ws://192.168.1.147:8282');
    var div = $('#content');
    var nickname = $('#nickname');
    socket.onopen = function () {
        var hello = {
            type: 'login',
            nickname: nickname.val()
        };
        socket.send(JSON.stringify(hello));
        socket.onmessage = function (event) {
            var message = eval("(" + event.data + ")");
            if (message.type == 'login') {
                div.prepend("<div class='my-mess login animated bounceInRight' ><strong>" + message.nickname + "登录了！</strong></div>");
            } else if (message.type == 'say') {
                div.prepend("<div class='my-mess say animated fadeIn' ><strong>" + message.nickname + "说：</strong> " + message.say + "</div>");
            } else if (message.type == 'logout') {
                div.prepend("<div class='my-mess logout animated bounceInRight' ><strong>" + message.nickname + "退出了！</strong></div>");
            } else if (message.type == 'signUp') {
                div.prepend("<div class='my-mess signUp animated bounceInRight' ><strong>" + message.nickname + "报名了！</strong></div>");
            }
            var tr_obj = $('.my-mess');
            if (tr_obj.length >= 20) {
                tr_obj.eq(tr_obj.length - 1).remove();
            }
        };
        socket.onclose = function (event) {
            console.log('Client notified socket has closed', event);
        };
    };


    var send_content = function () {
        $('#send-btn').click(function () {
            var obj = $('#send_content');
            var say = {
                type: 'say',
                say: obj.val()
            };
            socket.send(JSON.stringify(say));
            obj.val('');
        });
    };

    var get_sign_up_list = function () {
        var sign_up_list_obj = $('#sign-up-list');
        console.log(sign_up_list_obj.data('url'));
        $.get(sign_up_list_obj.data('url'), function (res) {
            if ('' == res) {
                return 0;
            } else {
                var html = '';
                $.each(JSON.parse(res), function (n, value) {
                    html += '<li class="list-group-item">' + value.nickname + '</li>';
                });
                sign_up_list_obj.html(html);
            }
        });
    };

    var do_sign_up = function () {
        $('.flash-no-time').click(function () {
            var obj = $(this);
            var url = obj.data('url');
            var param = {
                nickname: nickname.val()
            };

            $.post(url, param, function (res) {
                if ('ok' == res) {
                    get_sign_up_list();
                    var signUp = {
                        type: 'signUp',
                        nickname: nickname.val()
                    };
                    socket.send(JSON.stringify(signUp));
                }
            });
        });
    };

    send_content();
    get_sign_up_list();
    do_sign_up();
    setInterval(get_sign_up_list, 5111);

    var qiu_obj = $('#qiu');
    var car_obj = $('#car');
    var qiu_init = function () {
        qiu_obj.animate(
            {marginTop: [0, 'easeOutQuad'], marginLeft: [150, 'linear']},
            800
        ).animate(
            {marginTop: [100, 'easeInQuad'], marginLeft: [300, 'linear']},
            800
        ).animate(
            {marginTop: [200, 'easeOutBounce'], marginLeft: [400, 'linear']},
            900
        );
    };

    var car_come = function () {
        car_obj.animate({
            marginLeft: 200,
            opacity: 1
        }, 500, 'linear').animate({marginLeft: 700}, 1500, 'linear').animate({
            marginLeft: 1200,
            opacity: 0
        }, 3000, 'linear');
    };
    var qiu_fei = function () {
        qiu_obj.animate({marginLeft: 800}, 500,'linear').animate({marginLeft: 1200, opacity: 0}, 500,'linear');
    };
    qiu_init();
    setTimeout(car_come, 2600);
    setTimeout(qiu_fei, 3560);

    var var_get_wav = function () {
        $(document).on('click','.my-mess',function(){
            var div_obj = $(this);
            $.post($("#get-wav-url").val() , {content:div_obj.html()} , function(res){
                if(0 == res){
                    return;
                }else{
                    var html = '<audio src="' + res + '" style="display:none " autoplay="autoplay" loop="false"></audio>';
                    $('#wav').append(html);
                }
            });
        });
    };
    var_get_wav();
})(jQuery);