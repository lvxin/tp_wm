<?php
/**
 * Created by PhpStorm.
 * User: LENOVO
 * Date: 2016/6/2
 * Time: 16:18
 */

namespace Home\Controller;


use Think\Controller;

class BaseController extends Controller
{

    protected $nickname;
    protected $activity;

    public function _initialize(){
        $this->nickname = session('nickname');
        $this->activity = $this->getNowActivityInfo();
    }


    protected function getNowActivityInfo(){
        $activity = M('activity')
            ->where('is_del = 0')
            ->order('id DESC')
            ->find();
        return $activity;
    }
}