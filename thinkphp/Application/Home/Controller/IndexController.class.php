<?php
namespace Home\Controller;



class IndexController extends BaseController
{

    /*首页*/
    public function index(){
        if(empty($this->nickname)){
            redirect(U('Home/index/setNickname'));
        }
        //活动信息
        $this->assign('_activity',$this->activity);
        $this->display();
    }

    public function setNickname(){
        $this->display('nickname');
    }

    public function saveNickname(){
        $nickname = I('nickname');
        if(!empty($nickname)){
            session('nickname',$nickname);
        }
        redirect(U('Home/Index/index'));
    }

    public function tts(){
        $content = strip_tags($_POST['content']);
        $file_name = md5(time()) . '.ogg';
        $shell = "ekho '{$content}' -o " . TTS_TMP_PATH . $file_name . " -t ogg";
        exec($shell);
        echo substr(TTS_TMP_PATH . $file_name, 1);
    }
}