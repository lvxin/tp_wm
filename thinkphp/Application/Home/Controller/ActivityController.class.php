<?php
/**
 * Created by PhpStorm.
 * User: LENOVO
 * Date: 2016/6/2
 * Time: 16:16
 */

namespace Home\Controller;


class ActivityController extends BaseController
{

    /* 报名 */
    public function doSignUp(){
        $nickname = I('nickname');
        $dao = M('sign_up_link');
        $activity = $this->activity;
        $link = $dao->where("nickname LIKE '{$nickname}' AND activity_id = {$activity['id']}")->find();
        if(empty($link)){
            $dao->add(array('activity_id' => $activity['id'],'nickname' => $nickname,'create_time' => time()));
            echo 'ok';
        }else{
            echo 'error';
        }
    }

    public function getSignUpList(){
        $activity = $this->activity;
        $linkList = M('sign_up_link')
            ->where("activity_id = {$activity['id']}")
            ->select();
        echo json_encode($linkList);
    }
}